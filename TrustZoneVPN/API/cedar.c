//
//  cedar.c
//  VPN
//
//  Created by Konstantin Chukhas on 7/10/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

#include "cedar.h"
/*
 bool IsBigEndian()
 {
 unsigned short test;
 UCHAR *buf;
 
 static int g_result = -1;
 
 return true;
 
 if (g_result != -1)
 {
 return g_result;
 }
 
 test = 0x1234;
 buf = (UCHAR *)&test;
 if (buf[0] == 0x12)
 {
 g_result = 0;
 }
 else
 {
 g_result = 1;
 }
 
 return g_result;
 }
 */
void Add(LIST *o, void *p)
{
    unsigned int i;
    // Validate arguments
    if (o == NULL || p == NULL)
    {
        return;
    }
    
    i = o->num_item;
    o->num_item++;
    
    if (o->num_item > o->num_reserved)
    {
        o->num_reserved = o->num_reserved * 2;
        o->p = realloc(o->p, sizeof(void *) * o->num_reserved);
    }
    
    o->p[i] = p;
    
}

LIST *NewList()
{
    LIST *o;
    
    o = malloc(sizeof(LIST));
    
    o->num_item = 0;
    o->num_reserved = 10;
    
    o->p = malloc(sizeof(void *) * o->num_reserved);
    
    return o;
}

void FreeList(LIST *o)
{
    // Validate arguments
    if (o == NULL)
    {
        return;
    }
    
    free(o->p);
    free(o);
}


BUF *NewBuf()
{
    BUF *b;
    
    // Memory allocation
    b = malloc(sizeof(BUF));
    b->buf = malloc(10);
    b->size = 0;
    b->current = 0;
    b->size_reserved = 10;
    return b;
}

void FreeBuf(BUF *b)
{
    // Validate arguments
    if (b == NULL)
    {
        return;
    }
    
    // Memory release
    free(b->buf);
    free(b);
    
}

void AdjustBufSize(BUF *b, UINT new_size)
{
    // Validate arguments
    if (b == NULL)
    {
        return;
    }
    
    if (b->size_reserved >= new_size)
    {
        return;
    }
    
    while (b->size_reserved < new_size)
    {
        b->size_reserved = b->size_reserved * 2;
    }
    b->buf = realloc(b->buf, b->size_reserved);
}

// Write to the buffer
void WriteBuf(BUF *b, void *buf, UINT size)
{
    UINT new_size;
    // Validate arguments
    if (b == NULL || buf == NULL || size == 0)
    {
        return;
    }
    
    new_size = b->current + size;
    if (new_size > b->size)
    {
        // Adjust the size
        AdjustBufSize(b, new_size);
    }
    
    if (b->buf != NULL)
    {
        memcpy((UCHAR *)b->buf + b->current, buf, size);
    }
    b->current += size;
    b->size = new_size;
    
}

void AddBufStr(BUF *b, char *str)
{
    // Validate arguments
    if (b == NULL || str == NULL)
    {
        return;
    }
    
    WriteBuf(b, str, strlen(str));
}

void SeekBuf(BUF *b, UINT offset, int mode)
{
    UINT new_pos;
    // Validate arguments
    if (b == NULL)
    {
        return;
    }
    
    if (mode == 0)
    {
        // Absolute position
        new_pos = offset;
    }
    else
    {
        if (mode > 0)
        {
            // Move Right
            new_pos = b->current + offset;
        }
        else
        {
            // Move Left
            if (b->current >= offset)
            {
                new_pos = b->current - offset;
            }
            else
            {
                new_pos = 0;
            }
        }
    }
    b->current = MAKESURE(new_pos, 0, b->size);
}

// Release of the PACK object
void FreePack(PACK *p)
{
    UINT i;
    ELEMENT **elements;
    // Validate arguments
    if (p == NULL)
    {
        return;
    }
    
    elements = ToArray(p->elements);
    for (i = 0;i < LIST_NUM(p->elements);i++)
    {
        FreeElement(elements[i]);
    }
    free(elements);
    
    FreeList(p->elements);
    free(p);
}

PACK *BufToPack(BUF *b)
{
    PACK *p;
    // Validate arguments
    if (b == NULL)
    {
        return NULL;
    }
    
    p = NewPack();
    if (ReadPack(b, p) == false)
    {
        FreePack(p);
        return NULL;
    }
    
    return p;
}

// Create a PACK object
PACK *NewPack()
{
    PACK *p;
    
    // Memory allocation
    p = (PACK *)malloc(sizeof(PACK));
    memset(p,0,sizeof(PACK));
    
    // Creating a List
    p->elements = NewList();
    
    return p;
}


void *ToArray(LIST *o)
{
    void *p;
    // Validate arguments
    if (o == NULL)
    {
        return NULL;
    }
    
    // Memory allocation
    p = malloc(sizeof(void *) * LIST_NUM(o));
    // Copy
    CopyToArray(o, p);
    
    return p;
}

void CopyToArray(LIST *o, void *p)
{
    // Validate arguments
    if (o == NULL || p == NULL)
    {
        return;
    }
    
    // KS
    
    memcpy(p, o->p, sizeof(void *) * o->num_item);
}

void FreeElement(ELEMENT *e)
{
    UINT i;
    // Validate arguments
    if (e == NULL)
    {
        return;
    }
    
    for (i = 0;i < e->num_value;i++)
    {
        FreeValue(e->values[i], e->type);
    }
    
    free(e->values);
    free(e);
}

void FreeValue(VALUE *v, UINT type)
{
    // Validate arguments
    if (v == NULL)
    {
        return;
    }
    
    switch (type)
    {
        case VALUE_INT:
        case VALUE_INT64:
            break;
        case VALUE_DATA:
            free(v->Data);
            break;
        case VALUE_STR:
            free(v->Str);
            break;
        case VALUE_UNISTR:
            free(v->UniStr);
            break;
    }
    
    // Memory release
    free(v);
}

bool ReadPack(BUF *b, PACK *p)
{
    UINT i, num;
    // Validate arguments
    if (b == NULL || p == NULL)
    {
        return false;
    }
    
    // The number of ELEMENTs
    num = ReadBufInt(b);
    if (num > MAX_ELEMENT_NUM)
    {
        // Number exceeds
        return false;
    }
    
    // Read the ELEMENT
    for (i = 0;i < num;i++)
    {
        ELEMENT *e;
        e = ReadElement(b);
        if (AddElement(p, e) == false)
        {
            // Adding error
            return false;
        }
    }
    
    return true;
}


// Read an integer from the buffer
UINT ReadBufInt(BUF *b)
{
    UINT value;
    // Validate arguments
    if (b == NULL)
    {
        return 0;
    }
    
    if (ReadBuf(b, &value, sizeof(UINT)) != sizeof(UINT))
    {
        return 0;
    }
    return Endian32(value);
}

// Endian conversion 32bit
UINT Endian32(UINT src)
{
    int x = 1;
    if (*((char *)&x))
    {
        return Swap32(src);
    }
    else
    {
        return src;
    }
}

// 32bit swap
UINT Swap32(UINT value)
{
    UINT r;
    ((BYTE *)&r)[0] = ((BYTE *)&value)[3];
    ((BYTE *)&r)[1] = ((BYTE *)&value)[2];
    ((BYTE *)&r)[2] = ((BYTE *)&value)[1];
    ((BYTE *)&r)[3] = ((BYTE *)&value)[0];
    return r;
}


UINT ReadBuf(BUF *b, void *buf, UINT size)
{
    UINT size_read;
    // Validate arguments
    if (b == NULL || size == 0)
    {
        return 0;
    }
    
    if (b->buf == NULL)
    {
        Zero(buf, size);
        return 0;
    }
    size_read = size;
    if ((b->current + size) >= b->size)
    {
        size_read = b->size - b->current;
        if (buf != NULL)
        {
            Zero((UCHAR *)buf + size_read, size - size_read);
        }
    }
    
    if (buf != NULL)
    {
        memcpy(buf, (UCHAR *)b->buf + b->current, size_read);
    }
    
    b->current += size_read;
    
    // KS
    
    return size_read;
}

void Zerro(void *addr, UINT size)
{
    memset(addr, 0, size);
}

void Zero(void *addr, UINT size)
{
    memset(addr, 0, size);
}


ELEMENT *ReadElement(BUF *b)
{
    UINT i;
    char name[MAX_ELEMENT_NAME_LEN + 1];
    UINT type, num_value;
    VALUE **values;
    ELEMENT *e;
    // Validate arguments
    if (b == NULL)
    {
        return NULL;
    }
    
    // Name
    if (ReadBufStr(b, name, sizeof(name)) == false)
    {
        return NULL;
    }
    
    // Type of item
    type = ReadBufInt(b);
    
    // Number of items
    num_value = ReadBufInt(b);
    if (num_value > MAX_VALUE_NUM)
    {
        // Number exceeds
        return NULL;
    }
    
    // VALUE
    values = (VALUE **)malloc(sizeof(VALUE *) * num_value);
    for (i = 0;i < num_value;i++)
    {
        values[i] = ReadValue(b, type);
    }
    
    // Create a ELEMENT
    e = NewElement(name, type, num_value, values);
    
    free(values);
    
    return e;
}


bool ReadBufStr(BUF *b, char *str, UINT size)
{
    UINT len;
    UINT read_size;
    // Validate arguments
    if (b == NULL || str == NULL || size == 0)
    {
        return false;
    }
    
    // Read the length of the string
    len = ReadBufInt(b);
    if (len == 0)
    {
        return false;
    }
    len--;
    if (len <= (size - 1))
    {
        size = len + 1;
    }
    
    read_size = MIN(len, (size - 1));
    
    // Read the string body
    if (ReadBuf(b, str, read_size) != read_size)
    {
        return false;
    }
    if (read_size < len)
    {
        ReadBuf(b, NULL, len - read_size);
    }
    str[read_size] = 0;
    
    return true;
}

// Read the VALUE
VALUE *ReadValue(BUF *b, UINT type)
{
    UINT len;
    BYTE *u;
    void *data;
    char *str;
    wchar_t *unistr;
    UINT unistr_size;
    UINT size;
    UINT u_size;
    VALUE *v = NULL;
    // Validate arguments
    if (b == NULL)
    {
        return NULL;
    }
    
    // Data item
    switch (type)
    {
        case VALUE_INT:            // Integer
            v = NewIntValue(ReadBufInt(b));
            break;
        case VALUE_INT64:
            v = NewInt64Value(ReadBufInt64(b));
            break;
        case VALUE_DATA:        // Data
            size = ReadBufInt(b);
            if (size > MAX_VALUE_SIZE)
            {
                // Size over
                break;
            }
            data = malloc(size);
            if (ReadBuf(b, data, size) != size)
            {
                // Read failure
                free(data);
                break;
            }
            v = NewDataValue(data, size);
            free(data);
            break;
        case VALUE_STR:            // ANSI string
            len = ReadBufInt(b);
            if ((len + 1) > MAX_VALUE_SIZE)
            {
                // Size over
                break;
            }
            str = malloc(len + 1);
            // String body
            if (ReadBuf(b, str, len) != len)
            {
                // Read failure
                free(str);
                break;
            }
            str[len] = 0;
            v = NewStrValue(str);
            free(str);
            break;
        case VALUE_UNISTR:        // Unicode string
            u_size = ReadBufInt(b);
            if (u_size > MAX_VALUE_SIZE)
            {
                // Size over
                break;
            }
            // Reading an UTF-8 string
            u = ZeroMalloc(u_size + 1);
            if (ReadBuf(b, u, u_size) != u_size)
            {
                // Read failure
                free(u);
                break;
            }
            // Convert to a Unicode string
            unistr_size = CalcUtf8ToUni(u, u_size);
            if (unistr_size == 0)
            {
                free(u);
                break;
            }
            unistr = malloc(unistr_size);
            Utf8ToUni(unistr, unistr_size, u, u_size);
            free(u);
            v = NewUniStrValue(unistr);
            free(unistr);
            break;
    }
    
    return v;
}


// Create a VALUE of Unicode String type
VALUE *NewUniStrValue(wchar_t *str)
{
    VALUE *v;
    // Validate arguments
    if (str == NULL)
    {
        return NULL;
    }
    
    // Memory allocation
    v = malloc(sizeof(VALUE));
    
    // String copy
    v->Size = UniStrSize(str);
    v->UniStr = malloc(v->Size);
    UniStrCpy(v->UniStr, v->Size, str);
    
    UniTrim(v->UniStr);
    
    return v;
}

// Creation of the VALUE of ANSI string type
VALUE *NewStrValue(char *str)
{
    VALUE *v;
    // Validate arguments
    if (str == NULL)
    {
        return NULL;
    }
    
    // Memory allocation
    v = malloc(sizeof(VALUE));
    
    // String copy
    v->Size = strlen(str) + 1;
    v->Str = malloc(v->Size);
    StrCpy(v->Str, v->Size, str);
    
    Trim(v->Str);
    
    return v;
}

// Create the VALUE of the data type
VALUE *NewDataValue(void *data, UINT size)
{
    VALUE *v;
    // Validate arguments
    if (data == NULL)
    {
        return NULL;
    }
    
    // Memory allocation
    v = malloc(sizeof(VALUE));
    
    // Data copy
    v->Size = size;
    v->Data = malloc(v->Size);
    memcpy(v->Data, data, size);
    
    return v;
}

// Create the VALUE of 64 bit integer type
VALUE *NewInt64Value(UINT64 i)
{
    VALUE *v;
    
    v = malloc(sizeof(VALUE));
    v->Int64Value = i;
    v->Size = sizeof(UINT64);
    
    return v;
}

// Create the VALUE of integer type
VALUE *NewIntValue(UINT i)
{
    VALUE *v;
    
    // Memory allocation
    v = malloc(sizeof(VALUE));
    v->IntValue = i;
    v->Size = sizeof(UINT);
    
    return v;
}

void *ZerroMalloc(UINT size)
{
    void *p;
    p = malloc(size);
    Zerro(p,size);
    return p;
}


UINT UniStrSize(wchar_t *str)
{
    // Validate arguments
    if (str == NULL)
    {
        return 0;
    }
    
    return (UniStrLen(str) + 1) * sizeof(wchar_t);
}


void Trim(char *str)
{
    // Validate arguments
    if (str == NULL)
    {
        return;
    }
    
    // Trim on the left side
    TrimLeft(str);
    
    // Trim on the right side
    TrimRight(str);
}

// Remove white spaces on the right side of the string
void TrimRight(char *str)
{
    char *buf, *tmp;
    UINT len, i, wp, wp2;
    bool flag;
    // Validate arguments
    if (str == NULL)
    {
        return;
    }
    len = strlen(str);
    if (len == 0)
    {
        return;
    }
    if (str[len - 1] != ' ' && str[len - 1] != '\t')
    {
        return;
    }
    
    buf = malloc(len + 1);
    tmp = malloc(len + 1);
    flag = false;
    wp = 0;
    wp2 = 0;
    for (i = 0;i < len;i++)
    {
        if (str[i] != ' ' && str[i] != '\t')
        {
            memcpy(buf + wp, tmp, wp2);
            wp += wp2;
            wp2 = 0;
            buf[wp++] = str[i];
        }
        else
        {
            tmp[wp2++] = str[i];
        }
    }
    buf[wp] = 0;
    StrCpy(str, 0, buf);
    free(buf);
    free(tmp);
}

// Remove white spaces from the left side of the string
void TrimLeft(char *str)
{
    char *buf;
    UINT len, i, wp;
    bool flag;
    // Validate arguments
    if (str == NULL)
    {
        return;
    }
    len = strlen(str);
    if (len == 0)
    {
        return;
    }
    if (str[0] != ' ' && str[0] != '\t')
    {
        return;
    }
    
    buf = malloc(len + 1);
    flag = false;
    wp = 0;
    for (i = 0;i < len;i++)
    {
        if (str[i] != ' ' && str[i] != '\t')
        {
            flag = true;
        }
        if (flag)
        {
            buf[wp++] = str[i];
        }
    }
    buf[wp] = 0;
    StrCpy(str, 0, buf);
    free(buf);
}


UINT StrCpy(char *dst, UINT size, char *src)
{
    UINT len;
    // Validate arguments
    if (dst == src)
    {
        return strlen(src);
    }
    if (dst == NULL || src == NULL)
    {
        if (src == NULL && dst != NULL)
        {
            if (size >= 1)
            {
                dst[0] = '\0';
            }
        }
        return 0;
    }
    if (size == 1)
    {
        dst[0] = '\0';
        return 0;
    }
    if (size == 0)
    {
        // Ignore the length
        size = 0x7fffffff;
    }
    
    // Check the length
    len = strlen(src);
    if (len <= (size - 1))
    {
        memcpy(dst, src, len + 1);
    }
    else
    {
        len = size - 1;
        memcpy(dst, src, len);
        dst[len] = '\0';
    }
    
    // KS
    
    return len;
}

void Copy(void *dst, void *src, UINT size)
{
    memcpy(dst,src,size);
}

UINT UniStrLen(wchar_t *str)
{
    UINT i;
    // Validate arguments
    if (str == NULL)
    {
        return 0;
    }
    
    i = 0;
    while (true)
    {
        if (str[i] == 0)
        {
            break;
        }
        i++;
    }
    
    return i;
}

UINT UniStrCpy(wchar_t *dst, UINT size, wchar_t *src)
{
    UINT len;
    // Validate arguments
    if (dst == NULL || src == NULL)
    {
        if (src == NULL && dst != NULL)
        {
            if (size >= sizeof(wchar_t))
            {
                dst[0] = L'\0';
            }
        }
        return 0;
    }
    if (dst == src)
    {
        return UniStrLen(src);
    }
    if (size != 0 && size < sizeof(wchar_t))
    {
        return 0;
    }
    if (size == sizeof(wchar_t))
    {
        wcscpy(dst, L"");
        return 0;
    }
    if (size == 0)
    {
        // Ignore the length
        size = 0x3fffffff;
    }
    
    // Check the length
    len = UniStrLen(src);
    if (len <= (size / sizeof(wchar_t) - 1))
    {
        Copy(dst, src, (len + 1) * sizeof(wchar_t));
    }
    else
    {
        len = size / sizeof(wchar_t) - 1;
        Copy(dst, src, len * sizeof(wchar_t));
        dst[len] = 0;
    }
    
    return len;
}


void UniTrim(wchar_t *str)
{
    // Validate arguments
    if (str == NULL)
    {
        return;
    }
    
    UniTrimLeft(str);
    UniTrimRight(str);
}

// Remove white space on the right side of the string
void UniTrimRight(wchar_t *str)
{
    wchar_t *buf, *tmp;
    UINT len, i, wp, wp2;
    bool flag;
    // Validate arguments
    if (str == NULL)
    {
        return;
    }
    len = UniStrLen(str);
    if (len == 0)
    {
        return;
    }
    if (str[len - 1] != L' ' && str[len - 1] != L'\t')
    {
        return;
    }
    
    buf = Malloc((len + 1) * sizeof(wchar_t));
    tmp = Malloc((len + 1) * sizeof(wchar_t));
    flag = false;
    wp = wp2 = 0;
    for (i = 0;i < len;i++)
    {
        if (str[i] != L' ' && str[i] != L'\t')
        {
            Copy(&buf[wp], tmp, wp2 * sizeof(wchar_t));
            wp += wp2;
            wp2 = 0;
            buf[wp++] = str[i];
        }
        else
        {
            tmp[wp2++] = str[i];
        }
    }
    buf[wp] = 0;
    UniStrCpy(str, 0, buf);
    free(buf);
    free(tmp);
}

// Remove white space from the left side of the string
void UniTrimLeft(wchar_t *str)
{
    wchar_t *buf;
    UINT len, i, wp;
    bool flag;
    // Validate arguments
    if (str == NULL)
    {
        return;
    }
    len = UniStrLen(str);
    if (len == 0)
    {
        return;
    }
    if (str[0] != L' ' && str[0] != L'\t')
    {
        return;
    }
    
    buf = Malloc((len + 1) * sizeof(wchar_t));
    flag = false;
    wp = 0;
    for (i = 0;i < len;i++)
    {
        if (str[i] != L' ' && str[i] != L'\t')
        {
            flag = true;
        }
        if (flag)
        {
            buf[wp++] = str[i];
        }
    }
    buf[wp] = 0;
    UniStrCpy(str, 0, buf);
    free(buf);
}

void *Malloc(UINT size)
{
    return malloc(size);
}

UINT64 ReadBufInt64(BUF *b)
{
    UINT64 value;
    // Validate arguments
    if (b == NULL)
    {
        return 0;
    }
    
    if (ReadBuf(b, &value, sizeof(UINT64)) != sizeof(UINT64))
    {
        return 0;
    }
    return Endian64(value);
}

UINT64 Endian64(UINT64 src)
{
    int x = 1;
    if (*((char *)&x))
    {
        return Swap64(src);
    }
    else
    {
        return src;
    }
}

UINT64 Swap64(UINT64 value)
{
    UINT64 r;
    ((BYTE *)&r)[0] = ((BYTE *)&value)[7];
    ((BYTE *)&r)[1] = ((BYTE *)&value)[6];
    ((BYTE *)&r)[2] = ((BYTE *)&value)[5];
    ((BYTE *)&r)[3] = ((BYTE *)&value)[4];
    ((BYTE *)&r)[4] = ((BYTE *)&value)[3];
    ((BYTE *)&r)[5] = ((BYTE *)&value)[2];
    ((BYTE *)&r)[6] = ((BYTE *)&value)[1];
    ((BYTE *)&r)[7] = ((BYTE *)&value)[0];
    return r;
}

void *ZeroMalloc(UINT size)
{
    void *p = malloc(size);
    Zero(p, size);
    return p;
}

ELEMENT *NewElement(char *name, UINT type, UINT num_value, VALUE **values)
{
    ELEMENT *e;
    UINT i;
    // Validate arguments
    if (name == NULL || num_value == 0 || values == NULL)
    {
        return NULL;
    }
    
    // Memory allocation
    e = Malloc(sizeof(ELEMENT));
    StrCpy(e->name, sizeof(e->name), name);
    e->num_value = num_value;
    e->type = type;
    
    // Copy of the pointer list to the element
    e->values = (VALUE **)Malloc(sizeof(VALUE *) * num_value);
    for (i = 0;i < e->num_value;i++)
    {
        e->values[i] = values[i];
    }
    
    return e;
}

bool AddElement(PACK *p, ELEMENT *e)
{
    // Validate arguments
    if (p == NULL || e == NULL)
    {
        return false;
    }
    
    // Size Check
    if (LIST_NUM(p->elements) >= MAX_ELEMENT_NUM)
    {
        // Can not add any more
        FreeElement(e);
        return false;
    }
    
    // Check whether there is another item which have same name
    if (GetElement(p, e->name, INFINITE))
    {
        // Exists
        FreeElement(e);
        return false;
    }
    
    if (e->num_value == 0)
    {
        // VALUE without any items can not be added
        FreeElement(e);
        return false;
    }
    
    // Adding
    Add(p->elements, e);
    return true;
}


ELEMENT *GetElement(PACK *p, char *name, UINT type)
{
    ELEMENT t;
    ELEMENT *e;
    // Validate arguments
    if (p == NULL || name == NULL)
    {
        return NULL;
    }
    
    // Search
    StrCpy(t.name, sizeof(t.name), name);
    e = Search(p->elements, &t);
    
    if (e == NULL)
    {
        return NULL;
    }
    
    // Type checking
    if (type != INFINITE)
    {
        if (e->type != type)
        {
            return NULL;
        }
    }
    
    return e;
}

ELEMENT *Search(LIST *o, ELEMENT *target)
{
    int i,num;
    // Validate arguments
    if (o == NULL || target == NULL)
    {
        return NULL;
    }
    
    for (i=0,num=LIST_NUM(o);i<num;i++)
    {
        ELEMENT *e = (ELEMENT *)LIST_DATA(o,i);
        if (StrCmp(e->name, target->name) == 0)
            return e;
    }
    
    return NULL;
}

UINT CalcUtf8ToUni(BYTE *u, UINT u_size)
{
    // Validate arguments
    if (u == NULL)
    {
        return 0;
    }
    if (u_size == 0)
    {
        u_size = StrLen((char *)u);
    }
    
    return (Utf8Len(u, u_size) + 1) * sizeof(wchar_t);
}

UINT StrLen(char *str)
{
    return strlen(str);
}

UINT Utf8Len(BYTE *u, UINT size)
{
    UINT i, num;
    // Validate arguments
    if (u == NULL)
    {
        return 0;
    }
    if (size == 0)
    {
        size = StrLen((char *)u);
    }
    
    i = num = 0;
    while (true)
    {
        UINT type;
        
        type = GetUtf8Type(u, size, i);
        if (type == 0)
        {
            break;
        }
        i += type;
        num++;
    }
    
    return num;
}

UINT GetUtf8Type(BYTE *s, UINT size, UINT offset)
{
    // Validate arguments
    if (s == NULL)
    {
        return 0;
    }
    if ((offset + 1) > size)
    {
        return 0;
    }
    if ((s[offset] & 0x80) == 0)
    {
        // 1 byte
        return 1;
    }
    if ((s[offset] & 0x20) == 0)
    {
        // 2 bytes
        if ((offset + 2) > size)
        {
            return 0;
        }
        return 2;
    }
    // 3 bytes
    if ((offset + 3) > size)
    {
        return 0;
    }
    return 3;
}

UINT Utf8ToUni(wchar_t *s, UINT size, BYTE *u, UINT u_size)
{
    UINT i, wp, num;
    // Validate arguments
    if (s == NULL || u == NULL)
    {
        return 0;
    }
    if (size == 0)
    {
        size = 0x3fffffff;
    }
    if (u_size == 0)
    {
        u_size = StrLen((char *)u);
    }
    
    i = 0;
    wp = 0;
    num = 0;
    while (true)
    {
        UINT type;
        wchar_t c;
        BYTE c1, c2;
        
        type = GetUtf8Type(u, u_size, i);
        if (type == 0)
        {
            break;
        }
        switch (type)
        {
            case 1:
                c1 = 0;
                c2 = u[i];
                break;
            case 2:
                c1 = (((u[i] & 0x1c) >> 2) & 0x07);
                c2 = (((u[i] & 0x03) << 6) & 0xc0) | (u[i + 1] & 0x3f);
                break;
            case 3:
                c1 = ((((u[i] & 0x0f) << 4) & 0xf0)) | (((u[i + 1] & 0x3c) >> 2) & 0x0f);
                c2 = (((u[i + 1] & 0x03) << 6) & 0xc0) | (u[i + 2] & 0x3f);
                break;
        }
        i += type;
        
        c = 0;
        
        if (IsBigEndian())
        {
            if (sizeof(wchar_t) == 2)
            {
                ((BYTE *)&c)[0] = c1;
                ((BYTE *)&c)[1] = c2;
            }
            else
            {
                ((BYTE *)&c)[2] = c1;
                ((BYTE *)&c)[3] = c2;
            }
        }
        else
        {
            ((BYTE *)&c)[0] = c2;
            ((BYTE *)&c)[1] = c1;
        }
        
        if (wp < ((size / sizeof(wchar_t)) - 1))
        {
            s[wp++] = c;
            num++;
        }
        else
        {
            break;
        }
    }
    
    if (wp < (size / sizeof(wchar_t)))
    {
        s[wp++] = 0;
    }
    
    return num;
}

UINT PackGetInt(PACK *p, char *name)
{
    return PackGetIntEx(p, name, 0);
}
UINT PackGetIntEx(PACK *p, char *name, UINT index)
{
    ELEMENT *e;
    // Validate arguments
    if (p == NULL || name == NULL)
    {
        return 0;
    }
    
    e = GetElement(p, name, VALUE_INT);
    if (e == NULL)
    {
        return 0;
    }
    return GetIntValue(e, index);
}

UINT GetIntValue(ELEMENT *e, UINT index)
{
    // Validate arguments
    if (e == NULL)
    {
        return 0;
    }
    if (index >= e->num_value)
    {
        return 0;
    }
    if (e->values[index] == NULL)
    {
        return 0;
    }
    
    return e->values[index]->IntValue;
}

int StrCmp(char *str1, char *str2)
{
    // Validate arguments
    if (str1 == NULL && str2 == NULL)
    {
        return 0;
    }
    if (str1 == NULL)
    {
        return 1;
    }
    if (str2 == NULL)
    {
        return -1;
    }
    
    return strcmp(str1, str2);
}

BUF *PackToBuf(PACK *p)
{
    BUF *b;
    // Validate arguments
    if (p == NULL)
    {
        return NULL;
    }
    
    b = NewBuf();
    WritePack(b, p);
    
    return b;
}

void PackAddStr(PACK *p, char *name, char *str)
{
    VALUE *v;
    // Validate arguments
    if (p == NULL || name == NULL || str == NULL)
    {
        return;
    }
    
    v = NewStrValue(str);
    AddElement(p, NewElement(name, VALUE_STR, 1, &v));
}

void PackAddInt(PACK *p, char *name, UINT i)
{
    VALUE *v;
    // Validate arguments
    if (p == NULL || name == NULL)
    {
        return;
    }
    
    v = NewIntValue(i);
    AddElement(p, NewElement(name, VALUE_INT, 1, &v));
}

void WritePack(BUF *b, PACK *p)
{
    UINT i;
    // Validate arguments
    if (b == NULL || p == NULL)
    {
        return;
    }
    
    // The number of ELEMENTs
    WriteBufInt(b, LIST_NUM(p->elements));
    
    // Write the ELEMENT
    for (i = 0;i < LIST_NUM(p->elements);i++)
    {
        ELEMENT *e = LIST_DATA(p->elements, i);
        WriteElement(b, e);
    }
}

bool WriteBufInt(BUF *b, UINT value)
{
    // Validate arguments
    if (b == NULL)
    {
        return false;
    }
    
    value = Endian32(value);
    
    WriteBuf(b, &value, sizeof(UINT));
    return true;
}

void WriteElement(BUF *b, ELEMENT *e)
{
    UINT i;
    // Validate arguments
    if (b == NULL || e == NULL)
    {
        return;
    }
    
    // Name
    WriteBufStr(b, e->name);
    // Type of item
    WriteBufInt(b, e->type);
    // Number of items
    WriteBufInt(b, e->num_value);
    // VALUE
    for (i = 0;i < e->num_value;i++)
    {
        VALUE *v = e->values[i];
        WriteValue(b, v, e->type);
    }
}

bool WriteBufStr(BUF *b, char *str)
{
    UINT len;
    // Validate arguments
    if (b == NULL || str == NULL)
    {
        return false;
    }
    
    // String length
    len = StrLen(str);
    if (WriteBufInt(b, len + 1) == false)
    {
        return false;
    }
    
    // String body
    WriteBuf(b, str, len);
    
    return true;
}

void WriteValue(BUF *b, VALUE *v, UINT type)
{
    UINT len;
    BYTE *u;
    UINT u_size;
    // Validate arguments
    if (b == NULL || v == NULL)
    {
        return;
    }
    
    // Data item
    switch (type)
    {
        case VALUE_INT:            // Integer
            WriteBufInt(b, v->IntValue);
            break;
        case VALUE_INT64:        // 64 bit integer
            WriteBufInt64(b, v->Int64Value);
            break;
        case VALUE_DATA:        // Data
            // Size
            WriteBufInt(b, v->Size);
            // Body
            WriteBuf(b, v->Data, v->Size);
            break;
        case VALUE_STR:            // ANSI string
            len = StrLen(v->Str);
            // Length
            WriteBufInt(b, len);
            // String body
            WriteBuf(b, v->Str, len);
            break;
        case VALUE_UNISTR:        // Unicode string
            // Convert to UTF-8
            u_size = CalcUniToUtf8(v->UniStr) + 1;
            u = ZeroMalloc(u_size);
            UniToUtf8(u, u_size, v->UniStr);
            // Size
            WriteBufInt(b, u_size);
            // UTF-8 string body
            WriteBuf(b, u, u_size);
            free(u);
            break;
    }
}


UINT CalcUniToUtf8(wchar_t *s)
{
    UINT i, len, size;
    // Validate arguments
    if (s == NULL)
    {
        return 0;
    }
    
    size = 0;
    len = UniStrLen(s);
    for (i = 0;i < len;i++)
    {
        size += GetUniType(s[i]);
    }
    
    return size;
}

UINT GetUniType(wchar_t c)
{
    BYTE c1, c2;
    
    if (IsBigEndian())
    {
        if (sizeof(wchar_t) == 2)
        {
            c1 = ((BYTE *)&c)[0];
            c2 = ((BYTE *)&c)[1];
        }
        else
        {
            c1 = ((BYTE *)&c)[2];
            c2 = ((BYTE *)&c)[3];
        }
    }
    else
    {
        c1 = ((BYTE *)&c)[1];
        c2 = ((BYTE *)&c)[0];
    }
    
    if (c1 == 0)
    {
        if (c2 <= 0x7f)
        {
            // 1 byte
            return 1;
        }
        else
        {
            // 2 bytes
            return 2;
        }
    }
    if ((c1 & 0xf8) == 0)
    {
        // 2 bytes
        return 2;
    }
    // 3 bytes
    return 3;
}

UINT UniToUtf8(BYTE *u, UINT size, wchar_t *s)
{
    UINT i, len, type, wp;
    // Validate arguments
    if (u == NULL || s == NULL)
    {
        return 0;
    }
    if (size == 0)
    {
        size = 0x3fffffff;
    }
    
    len = UniStrLen(s);
    wp = 0;
    for (i = 0;i < len;i++)
    {
        BYTE c1, c2;
        wchar_t c = s[i];
        
        if (IsBigEndian())
        {
            if (sizeof(wchar_t) == 2)
            {
                c1 = ((BYTE *)&c)[0];
                c2 = ((BYTE *)&c)[1];
            }
            else
            {
                c1 = ((BYTE *)&c)[2];
                c2 = ((BYTE *)&c)[3];
            }
        }
        else
        {
            c1 = ((BYTE *)&c)[1];
            c2 = ((BYTE *)&c)[0];
        }
        
        type = GetUniType(s[i]);
        switch (type)
        {
            case 1:
                if (wp < size)
                {
                    u[wp++] = c2;
                }
                break;
            case 2:
                if (wp < size)
                {
                    u[wp++] = 0xc0 | (((((c1 & 0x07) << 2) & 0x1c)) | (((c2 & 0xc0) >> 6) & 0x03));
                }
                if (wp < size)
                {
                    u[wp++] = 0x80 | (c2 & 0x3f);
                }
                break;
            case 3:
                if (wp < size)
                {
                    u[wp++] = 0xe0 | (((c1 & 0xf0) >> 4) & 0x0f);
                }
                if (wp < size)
                {
                    u[wp++] = 0x80 | (((c1 & 0x0f) << 2) & 0x3c) | (((c2 & 0xc0) >> 6) & 0x03);
                }
                if (wp < size)
                {
                    u[wp++] = 0x80 | (c2 & 0x3f);
                }
                break;
        }
    }
    if (wp < size)
    {
        u[wp] = 0;
    }
    return wp;
}


bool WriteBufInt64(BUF *b, UINT64 value)
{
    // Validate arguments
    if (b == NULL)
    {
        return false;
    }
    
    value = Endian64(value);
    
    WriteBuf(b, &value, sizeof(UINT64));
    return true;
}


bool PackGetUniStr(PACK *p, char *name, wchar_t *unistr, UINT size)
{
    return PackGetUniStrEx(p, name, unistr, size, 0);
}
bool PackGetUniStrEx(PACK *p, char *name, wchar_t *unistr, UINT size, UINT index)
{
    ELEMENT *e;
    // Validate arguments
    if (p == NULL || name == NULL || unistr == NULL || size == 0)
    {
        return false;
    }
    
    unistr[0] = 0;
    
    e = GetElement(p, name, VALUE_UNISTR);
    if (e == NULL)
    {
        return false;
    }
    UniStrCpy(unistr, size, GetUniStrValue(e, index));
    return true;
}

wchar_t *GetUniStrValue(ELEMENT *e, UINT index)
{
    // Validate arguments
    if (e == NULL)
    {
        return 0;
    }
    if (index >= e->num_value)
    {
        return 0;
    }
    if (e->values[index] == NULL)
    {
        return NULL;
    }
    
    return e->values[index]->UniStr;
}

UINT64 GetInt64Value(ELEMENT *e, UINT index)
{
    // Validate arguments
    if (e == NULL)
    {
        return 0;
    }
    if (index >= e->num_value)
    {
        return 0;
    }
    if (e->values[index] == NULL)
    {
        return 0;
    }
    
    return e->values[index]->Int64Value;
}

bool PackGetBool(PACK *p, char *name)
{
    return PackGetInt(p, name) == 0 ? false : true;
}
bool PackGetBoolEx(PACK *p, char *name, UINT index)
{
    return PackGetIntEx(p, name, index) == 0 ? false : true;
}

UINT64 PackGetInt64(PACK *p, char *name)
{
    return PackGetInt64Ex(p, name, 0);
}
UINT64 PackGetInt64Ex(PACK *p, char *name, UINT index)
{
    ELEMENT *e;
    // Validate arguments
    if (p == NULL || name == NULL)
    {
        return 0;
    }
    
    e = GetElement(p, name, VALUE_INT64);
    if (e == NULL)
    {
        return 0;
    }
    return GetInt64Value(e, index);
}

UINT64 SystemToLocal64(UINT64 t)
{
    SYSTEMTIME st;
    UINT64ToSystem(&st, t);
    SystemToLocal(&st, &st);
    return SystemToUINT64(&st);
}

void UINT64ToSystem(SYSTEMTIME *st, UINT64 sec64)
{
    UINT64 tmp64;
    UINT sec, millisec;
    time_t time;
    // Validate arguments
    if (st == NULL)
    {
        return;
    }
    
    tmp64 = sec64 / (UINT64)1000;
    millisec = (UINT)(sec64 - tmp64 * (UINT64)1000);
    sec = (UINT)tmp64;
    time = (time_t)sec;
    TimeToSystem(st, time);
    st->wMilliseconds = (WORD)millisec;
}

void TimeToSystem(SYSTEMTIME *st, time_t t)
{
    struct tm tmp;
    // Validate arguments
    if (st == NULL)
    {
        return;
    }
    
    TimeToTm(&tmp, t);
    TmToSystem(st, &tmp);
}

void TimeToTm(tm *t, time_t time)
{
    tm *ret;
    // Validate arguments
    if (t == NULL)
    {
        return;
    }
    
    ret = malloc(sizeof(tm));
    memset(ret, 0, sizeof(tm));
    gmtime_r(&time, ret);
    
    if (ret == NULL)
    {
        Zero(t, sizeof(tm));
    }
    else
    {
        Copy(t, ret, sizeof(tm));
    }
    
    free(ret);
}

void TmToSystem(SYSTEMTIME *st, tm *t)
{
    struct tm tmp;
    // Validate arguments
    if (st == NULL || t == NULL)
    {
        return;
    }
    
    Copy(&tmp, t, sizeof(tm));
    NormalizeTm(&tmp);
    
    Zero(st, sizeof(SYSTEMTIME));
    st->wYear = MAKESURE(tmp.tm_year + 1900, 1970, 2037);
    st->wMonth = MAKESURE(tmp.tm_mon + 1, 1, 12);
    st->wDay = MAKESURE(tmp.tm_mday, 1, 31);
    st->wDayOfWeek = MAKESURE(tmp.tm_wday, 0, 6);
    st->wHour = MAKESURE(tmp.tm_hour, 0, 23);
    st->wMinute = MAKESURE(tmp.tm_min, 0, 59);
    st->wSecond = MAKESURE(tmp.tm_sec, 0, 59);
    st->wMilliseconds = 0;
}

void NormalizeTm(tm *t)
{
    struct tm *ret;
    time_t tmp;
    // Validate arguments
    if (t == NULL)
    {
        return;
    }
    
    tmp = c_mkgmtime(t);
    if (tmp == (time_t)-1)
    {
        return;
    }
    
    ret = malloc(sizeof(tm));
    memset(ret, 0, sizeof(tm));
    gmtime_r(&tmp, ret);
    
    if (ret == NULL)
    {
        Zero(t, sizeof(tm));
    }
    else
    {
        Copy(t, ret, sizeof(tm));
    }
    
    free(ret);
}

void SystemToLocal(SYSTEMTIME *local, SYSTEMTIME *system)
{
    UINT64 sec64;
    // Validate arguments
    if (local == NULL || system == NULL)
    {
        return;
    }
    
    sec64 = (UINT64)((INT64)SystemToUINT64(system) + GetTimeDiffEx(system, false));
    UINT64ToSystem(local, sec64);
}

UINT64 SystemToUINT64(SYSTEMTIME *st)
{
    UINT64 sec64;
    time_t time;
    // Validate arguments
    if (st == NULL)
    {
        return 0;
    }
    
    time = SystemToTime(st);
    sec64 = (UINT64)time * (UINT64)1000;
    sec64 += st->wMilliseconds;
    
    return sec64;
}

time_t SystemToTime(SYSTEMTIME *st)
{
    tm t;
    // Validate arguments
    if (st == NULL)
    {
        return 0;
    }
    
    SystemToTm(&t, st);
    return TmToTime(&t);
}

void SystemToTm(tm *t, SYSTEMTIME *st)
{
    // Validate arguments
    if (t == NULL || st == NULL)
    {
        return;
    }
    
    Zero(t, sizeof(tm));
    t->tm_year = MAKESURE(st->wYear, 1970, 2037) - 1900;
    t->tm_mon = MAKESURE(st->wMonth, 1, 12) - 1;
    t->tm_mday = MAKESURE(st->wDay, 1, 31);
    t->tm_hour = MAKESURE(st->wHour, 0, 23);
    t->tm_min = MAKESURE(st->wMinute, 0, 59);
    t->tm_sec = MAKESURE(st->wSecond, 0, 59);
    
    t->tm_isdst = -1;
    NormalizeTm(t);
}

time_t c_mkgmtime(struct tm *tm)
{
    int years, months, days, hours, minutes, seconds;
    
    years = tm->tm_year + 1900;   /* year - 1900 -> year */
    months = tm->tm_mon;          /* 0..11 */
    days = tm->tm_mday - 1;       /* 1..31 -> 0..30 */
    hours = tm->tm_hour;          /* 0..23 */
    minutes = tm->tm_min;         /* 0..59 */
    seconds = tm->tm_sec;         /* 0..61 in ANSI C. */
    
    ADJUST_TM(seconds, minutes, 60);
    ADJUST_TM(minutes, hours, 60);
    ADJUST_TM(hours, days, 24);
    ADJUST_TM(months, years, 12);
    if (days < 0)
        do {
            if (--months < 0) {
                --years;
                months = 11;
            }
            days += monthlen(months, years);
        } while (days < 0);
    else
        while (days >= monthlen(months, years)) {
            days -= monthlen(months, years);
            if (++months >= 12) {
                ++years;
                months = 0;
            }
        }
    
    /* Restore adjusted values in tm structure */
    tm->tm_year = years - 1900;
    tm->tm_mon = months;
    tm->tm_mday = days + 1;
    tm->tm_hour = hours;
    tm->tm_min = minutes;
    tm->tm_sec = seconds;
    
    /* Set `days' to the number of days into the year. */
    days += ydays[months] + (months > 1 && leap (years));
    tm->tm_yday = days;
    
    /* Now calculate `days' to the number of days since Jan 1, 1970. */
    days = (unsigned)days + 365 * (unsigned)(years - 1970) +
    (unsigned)(nleap (years));
    tm->tm_wday = ((unsigned)days + 4) % 7; /* Jan 1, 1970 was Thursday. */
    tm->tm_isdst = 0;
    
    if (years < 1970)
        return (time_t)-1;
    
#if (defined(TM_YEAR_MAX) && defined(TM_MON_MAX) && defined(TM_MDAY_MAX))
#if (defined(TM_HOUR_MAX) && defined(TM_MIN_MAX) && defined(TM_SEC_MAX))
    if (years > TM_YEAR_MAX ||
        (years == TM_YEAR_MAX &&
         (tm->tm_yday > ydays[TM_MON_MAX] + (TM_MDAY_MAX - 1) +
          (TM_MON_MAX > 1 && leap (TM_YEAR_MAX)) ||
          (tm->tm_yday == ydays[TM_MON_MAX] + (TM_MDAY_MAX - 1) +
           (TM_MON_MAX > 1 && leap (TM_YEAR_MAX)) &&
           (hours > TM_HOUR_MAX ||
            (hours == TM_HOUR_MAX &&
             (minutes > TM_MIN_MAX ||
              (minutes == TM_MIN_MAX && seconds > TM_SEC_MAX) )))))))
        return (time_t)-1;
#endif
#endif
    
    return (time_t)(86400L * (unsigned long)(unsigned)days +
                    3600L * (unsigned long)hours +
                    (unsigned long)(60 * minutes + seconds));
}

time_t TmToTime(struct tm *t)
{
    time_t tmp;
    // Validate arguments
    if (t == NULL)
    {
        return 0;
    }
    
    tmp = c_mkgmtime(t);
    if (tmp == (time_t)-1)
    {
        return 0;
    }
    return tmp;
}

INT64 GetTimeDiffEx(SYSTEMTIME *basetime, bool local_time)
{
    time_t tmp;
    struct tm t1, t2;
    SYSTEMTIME snow;
    struct tm now;
    SYSTEMTIME s1, s2;
    INT64 ret;
    
    Copy(&snow, basetime, sizeof(SYSTEMTIME));
    
    SystemToTm(&now, &snow);
    if (local_time == false)
    {
        tmp = c_mkgmtime(&now);
    }
    else
    {
        tmp = mktime(&now);
    }
    
    if (tmp == (time_t)-1)
    {
        return 0;
    }
    
    localtime_r(&tmp, &t1);
    gmtime_r(&tmp, &t2);
    
    TmToSystem(&s1, &t1);
    TmToSystem(&s2, &t2);
    
    ret = (INT)SystemToUINT64(&s1) - (INT)SystemToUINT64(&s2);
    
    return ret;
}

UINT64 UniToInt64(wchar_t *str)
{
    char tmp[MAX_SIZE];
    // Validate arguments
    if (str == NULL)
    {
        return 0;
    }
    
    UniToStrForSingleChars(tmp, sizeof(tmp), str);
    
    return ToInt64(tmp);
}

void UniToStrForSingleChars(char *dst, UINT dst_size, wchar_t *src)
{
    UINT i;
    // Validate arguments
    if (dst == NULL || src == NULL)
    {
        return;
    }
    
    for (i = 0;i < UniStrLen(src) + 1;i++)
    {
        wchar_t s = src[i];
        char d;
        
        if (s == 0)
        {
            d = 0;
        }
        else if (s <= 0xff)
        {
            d = (char)s;
        }
        else
        {
            d = ' ';
        }
        
        dst[i] = d;
    }
}

UINT64 ToInt64(char *str)
{
    UINT len, i;
    UINT64 ret = 0;
    // Validate arguments
    if (str == NULL)
    {
        return 0;
    }
    
    len = StrLen(str);
    for (i = 0;i < len;i++)
    {
        char c = str[i];
        if (c != ',')
        {
            if ('0' <= c && c <= '9')
            {
                ret = ret * (UINT64)10 + (UINT64)(c - '0');
            }
            else
            {
                break;
            }
        }
    }
    
    return ret;
}


char *CopyUniToUtf(wchar_t *unistr)
{
    UINT size;
    char *ret;
    // Validate arguments
    if (unistr == NULL)
    {
        return NULL;
    }
    
    size = CalcUniToUtf8(unistr);
    ret = ZeroMalloc(size + sizeof(char));
    
    UniToUtf8((char *)ret, size, unistr);
    
    return ret;
}
