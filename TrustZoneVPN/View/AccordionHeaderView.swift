//
//  AccordionHeaderView.swift
//  EDCBlockchain
//
//  Created by Konstantin Chukhas on 5/7/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class AccordionHeaderView: UITableViewCell {
    static let kDefaultAccordionHeaderViewHeight: CGFloat = 44.0;
    static let kAccordionHeaderViewReuseIdentifier = "TableViewCellReuseIdentifier";
    
    @IBOutlet weak var arrowIcon: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.text = nil
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

