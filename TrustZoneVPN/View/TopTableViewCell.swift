//
//  TopTableViewCell.swift
//  EDCBlockchain
//
//  Created by Konstantin Chukhas on 5/7/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class TopTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var clCollactionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBOutlet weak var labelName: UILabel!
    override func prepareForReuse() {
        super.prepareForReuse()
        labelName = nil
        //set cell to initial state here
    }
}
//extension TopTableViewCell{
//    func setcollactionViewDataSourseDelegate< D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourseDelegate: D, forRow row :Int)
//    {
//        clCollactionView.delegate = dataSourseDelegate
//        clCollactionView.dataSource = dataSourseDelegate
//        clCollactionView.reloadData()
//
//    }
//}
