//
//  StatusVC.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/2/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit


class StatusVC: UIViewController {
    
    
    @IBOutlet weak var vpnStatusLabel: UILabel!
    @IBOutlet weak var serverIP: UILabel!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var portNameLabel: UILabel!
    @IBOutlet weak var expiresLabel: UILabel!
    @IBOutlet weak var dataTransferLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var autoconnectlabel: UILabel!
    @IBOutlet weak var vpnStatusImage: UIImageView!
    @IBOutlet weak var logoutButton: UIButton!
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15.0),
        NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.6677469611, green: 0.6677629352, blue: 0.6677542925, alpha: 1),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    var userDefaults = UserDefaults.standard
    var serversService = ServersService.self
    var servLine = [String]()
    var attributedString = NSMutableAttributedString(string:"")
    override func viewDidLoad() {
        super.viewDidLoad()
        //Button Logout
        let buttonTitleStr = NSMutableAttributedString(string:"(Logout)", attributes:attrs)
        attributedString.append(buttonTitleStr)
        logoutButton.setAttributedTitle(attributedString, for: .normal)
        
        // Do any additional setup after loading the view.
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        self.login.text = (userDefaults.object(forKey: "login") as! String)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkConectedToVpn()
       // let autoconnect = SettingsClass.share.activ ?? "Yes"
        self.expiresLabel.text = (UserDefaults.standard.object(forKey: "expiresTime") as! String)
        self.autoconnectlabel.text = userDefaults.object(forKey: "autoconnect") as? String ?? "Yes"
        }
    @objc func getIP()  {
        NetworkManager.sharedInstance.aa { (result) in
            self.servLine.append(result! as! String)
            for line in self.servLine {
                let serverProps = line.components(separatedBy: "|")
                if serverProps.count < 1 { continue }
               self.serverIP.text = "\(serverProps[1].lowercased()).trust.zone"
            }
        }
    }
    func checkConectedToVpn(){
        if isConnectedToVpn == true{
            let image: UIImage = UIImage(named:"online")!
            self.vpnStatusImage.image = image
            VPNMethod.share.vpnStatusLabel = "VPN ONLINE"
            getIP()
            self.vpnStatusLabel.text = VPNMethod.share.vpnStatusLabel
            self.vpnStatusLabel.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            self.serverIP.text = DomenName.sharedInstance.name ?? "vpn.trust.zone"
            
        }else{
            let image: UIImage = UIImage(named:"offline")!
            self.vpnStatusImage.image = image
            VPNMethod.share.vpnStatusLabel = "VPN OFFLINE"
//            getIP()
            self.vpnStatusLabel.text = VPNMethod.share.vpnStatusLabel
            self.vpnStatusLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.serverIP.text =  "vpn.trust.zone"
        }
    }
    private var isConnectedToVpn: Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") || key.contains("ipsec") {
                    return true
                }
            }
        }
        return false
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    @IBAction func RenewAction(_ sender: Any) {
        let pth = "https://trust.zone/order"
        if let url = URL(string: "\(pth)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func getServer()  {
        // GetServer
        let strsize = 10240;
        let strsizes = 10240;
        
        let serversStr = UnsafeMutablePointer<Int8>.allocate(capacity: strsize);
        let serversStrExpires = UnsafeMutablePointer<Int8>.allocate(capacity: strsize);
        let unsafePointerUserName = UnsafeMutablePointer<Int8>(mutating: (UserDefaults.standard.object(forKey: "login") as! NSString).utf8String)
        let unsafePointerLang = UnsafeMutablePointer<Int8>(mutating: ("EN" as NSString).utf8String)
        SslGetServerList(serversStr,  Int32(strsize),unsafePointerUserName, unsafePointerLang, serversStrExpires, Int32(strsizes))
        let expiresTime = String(cString: serversStrExpires)
         userDefaults.set(expiresTime, forKey: "expiresTime")
//        UserDefaults.standard.set(expiresTime, forKey: "expiresTime")
//        StatusStruct.share.ExpiresTime = expiresTime
//        let s:String  = String(cString: serversStr)
//        let s1:String  = String(cString: serversStrExpires)
////        print(s1)
////        print(s)
////
//        
//        let nameB = UnsafeMutablePointer<Int8>.allocate(capacity: strsizes);
//        SslGetServerList(serversStr,  Int32(strsize),unsafePointerUserName, unsafePointerLang, nameB, Int32(strsizes))
//        let name = String(cString: nameB)
//        print(name)
//        StatusStruct.share.ExpiresTime = name
//        userDefaults.set(name, forKey: "expiresTime")
    }
}
