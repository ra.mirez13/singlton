//
//  ExitVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import QuartzCore

class ExitVC: UIViewController {
   
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
          NSLog("viewDidLoad is running")
        // Do any additional setup after loading the view.
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    @IBAction func OkActionButton(_ sender: Any) {
         exit(0)
    }
    @IBAction func cancelActionButton(_ sender: Any) {
       
    }
}
