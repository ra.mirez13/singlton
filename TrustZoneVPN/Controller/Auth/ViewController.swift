//
//  ViewController.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/13/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import Darwin


class ViewController: UIViewController {
    
    @IBOutlet weak var loginTf: UITextField!
    @IBOutlet weak var passTf: UITextField!
    @IBOutlet weak var contactUsButton: UIButton!
    @IBOutlet weak var signIn: LoadingButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    //MARK:Variable
    var activ = true
    var popInfo = ""
    
    
    //MARK:Constants
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.systemFont(ofSize: 16),
        .foregroundColor: #colorLiteral(red: 0.00204864447, green: 0.3543668091, blue: 0.5375605226, alpha: 1),
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    let yourAttributesPass: [NSAttributedString.Key: Any] = [
        .font: UIFont.systemFont(ofSize: 16),
        .foregroundColor: #colorLiteral(red: 0.00204864447, green: 0.3543668091, blue: 0.5375605226, alpha: 1),
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    let userDefault = UserDefaults.standard
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegates()
        buttonLayers()
        buttonAtributes()
        
        
        //GetServer
        //        let strsize = 10240;
        //        let str = UnsafeMutablePointer<Int8>.allocate(capacity: strsize);
        //        let unsafePointerUserName = UnsafeMutablePointer<Int8>(mutating: ("ADMIN" as NSString).utf8String)
        //        let unsafePointerLang = UnsafeMutablePointer<Int8>(mutating: ("RU" as NSString).utf8String)
        //        SslGetServerList(str,  Int32(strsize),unsafePointerUserName, unsafePointerLang)
        //        let s:String  = String(cString: str);      //это отправка в функцию значение
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let savedData = userDefault.bool(forKey: "isLoggedIn")
        if(savedData){
            performSegue(withIdentifier: "segueTabBar", sender:nil)//here u have decide the which view will show if the user is logged in how. here i used   segue.
            self.tabBarController?.selectedIndex = 2
            
        }else{
            // this is the main view. just make the object of the class and called it.
        }
    }
    
    
    fileprivate func delegates() {
        // Do any additional setup after loading the view.
        loginTf.delegate = self
        passTf.delegate = self
    }
    
    fileprivate func buttonLayers() {
        loginTf.layer.cornerRadius = 10
        loginTf.layer.borderWidth = 2
        loginTf.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        passTf.layer.cornerRadius = 10
        passTf.layer.borderWidth = 2
        passTf.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    fileprivate func buttonAtributes() {
        //Button
        let attributeLogin = NSMutableAttributedString(string: "Sign up for free",attributes: yourAttributes)
        signUpButton.setAttributedTitle(attributeLogin, for: .normal)
        
        let attributePassword = NSMutableAttributedString(string: "Contact us",attributes: yourAttributesPass)
        contactUsButton.setAttributedTitle(attributePassword, for: .normal)
        
    }
    
    
    
    var arrayLogin = [String]()
    @IBAction func singInAction(_ sender: Any) {
        signIn.showLoading()
        _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { timer in
            self.signIn.hideLoading()
        })
        
        let unsafePointerLangLogin = UnsafeMutablePointer<Int8>(mutating: ("RU" as NSString).utf8String)
        let strsizeresult = 10240;
        let strresult = UnsafeMutablePointer<Int8>.allocate(capacity: strsizeresult);
        let unsafePointerUserNameLogin = UnsafeMutablePointer<Int8>(mutating: (self.loginTf.text! as NSString).utf8String)
        let unsafePointerLoginPass = UnsafeMutablePointer<Int8>(mutating: (self.passTf.text! as NSString).utf8String)
        
        SslLogin(strresult,  Int32(strsizeresult),unsafePointerUserNameLogin, unsafePointerLoginPass, unsafePointerLangLogin)
        let q  = String(cString: strresult)
        strresult.deallocate()
        
        
        if q.contains("|") {
            performSegue(withIdentifier: "segueTabBar", sender: nil)
            arrayLogin.append(q)
            userDefault.set(true, forKey: "isLoggedIn")
            userDefault.synchronize()
            for login in arrayLogin{
                let serverProps = login.components(separatedBy: "|")
                print(serverProps[0])
                print(serverProps[1])
                UserDefaults.standard.set(serverProps[0], forKey: "login")
                UserDefaults.standard.set(serverProps[1], forKey: "password")

            }
            
            
        }else{
            print(q)
            self.popInfo = q
            performSegue(withIdentifier: "mySegueID", sender: nil)
        }
        
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let popUpVC = segue.destination as? PopUpVC else {return}
        popUpVC.value = self.popInfo
    }
    @IBAction func singUpAction(_ sender: Any) {
        let pth = "https://trust.zone/registration"
        if let url = URL(string: "\(pth)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func contactUsAction(_ sender: Any) {
        let pth = "https://trust.zone/contact"
        if let url = URL(string: "\(pth)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
}
extension ViewController:UITextFieldDelegate{
    
    
}

