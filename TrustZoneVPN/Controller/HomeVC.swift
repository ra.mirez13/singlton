//
//  HomeVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import MBCircularProgressBar
import NetworkExtension
import KeychainSwift
import Security
import MaterialActivityIndicator
import QuartzCore
import Alamofire
import SystemConfiguration
import Reachability



class HomeVC: UIViewController {
    static let shared = HomeVC()
    
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var switchConntectionStatus: UISwitch!
    @IBOutlet weak var labelConntectionStatus: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var materialActivityIndicator: MaterialActivityIndicatorView!
    @IBOutlet weak var touchLabel: UILabel!
    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var ipImage: UIImageView!
    
    
    var vpnManager = NEVPNManager.shared()
    var isConnected = false
    var buttonSwitched : Bool = false
    var networkManager = NetworkManager()
    var servLine = [String]()
    var nameImage = ""
    var sender = Int()
    var doubleTap : Bool = false
    var stopVpn = false
    let reachability = Reachability()!
    private let keychain = KeychainService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.VPNStatusDidChange(_:)), name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        keychain.save(key: "vpn_password", value: UserDefaults.standard.object(forKey: "password") as! String)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getIP()
        _ = myFunction
        if DomenName.sharedInstance.domenSelected == true{
            domenNameDidChange(nil)
        }  }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getIP()
        self.reachabilityFunc()
        DomenName.sharedInstance.domenSelected = false
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getIP), userInfo: nil, repeats: true)
        checkConectedToVpn()
        VPNStatusDidChange(nil)
    }
    func checkConectedToVpn(){
        if isConnectedToVpn == true{
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage =  UIImage(named: "logo_done")!
                self.img.image =  yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                VPNMethod.share.vpnStatusImage = true
                UserDefaults.standard.set(true, forKey: "vpnStatus")
            })
            self.materialActivityIndicator.stopAnimating()
            self.materialActivityIndicator.isHidden = true
            self.materialActivityIndicator.lineWidth = 0
            self.progressView.stopRotating()
            
        }else{
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage =  UIImage(named: "ic_launcher")!
                self.img.image =  yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                VPNMethod.share.vpnStatusImage = false
                UserDefaults.standard.set(false, forKey: "vpnStatus")

            })
            
        }
    }
    private var isConnectedToVpn: Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") || key.contains("ipsec") {
                    return true
                }
            }
        }
        return false
    }
    
    private lazy var myFunction: Void = {
        // Do something once
        print("This should be executed only once during the lifetime of the program")
        if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true {
            if isConnectedToVpn == true{
                print("VPN is Activeted")
            }else{
                startVPN()
            }
        }
    }()
   
    
    
    @objc func getIP()  {
        checkConectedToVpn()
        NetworkManager.sharedInstance.aa { (result) in
            self.servLine.append(result! as! String)
            for line in self.servLine {
                let serverProps = line.components(separatedBy: "|")
                if serverProps.count < 1 { continue }
                self.ipLabel.text = serverProps[0]
                UserDefaults.standard.set(serverProps[0], forKey: "serverIp")
                let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
                self.ipImage.image = image
            }
        }
    }
  

    func switchClicked() {
   if !isConnected {
            initVPNTunnelProviderManager()
        }
        else{
            vpnManager.removeFromPreferences(completionHandler: { (error) in
                
                if((error) != nil) {
                    print("VPN Remove Preferences error: 1")
                    self.materialActivityIndicator.startAnimating()
                }
                else {
                    self.vpnManager.connection.stopVPNTunnel()
                    self.labelConntectionStatus.text = "Disconnected"
                    self.switchConntectionStatus.isOn = false
                    self.isConnected = false
                }
            })
        }
    }
    
    func initVPNTunnelProviderManager(){
        self.vpnManager.loadFromPreferences { [passwordRef = keychain.load(key: "vpn_password")] (error) -> Void in
            if((error) != nil) {
                print("VPN Preferences error: 1")
            }
            else {
                // You can change Protocol and credentials as per your protocol i.e IPSec or IKEv2
                let p = NEVPNProtocolIKEv2()
                p.username = (UserDefaults.standard.object(forKey: "login") as! String)
                p.remoteIdentifier = "trust.zone"
                p.serverAddress =  UserDefaults.standard.object(forKey: "domen") as? String ?? "vpn.trust.zone"
                p.useExtendedAuthentication = true
                p.disconnectOnSleep = false
                p.passwordReference = passwordRef
                p.authenticationMethod = .none
                p.enablePFS = true
                p.sharedSecretReference = passwordRef
                // Useful for when you have IPSec Protocol
                self.vpnManager.protocolConfiguration = p
                self.vpnManager.isEnabled = true
                self.vpnManager.saveToPreferences(completionHandler: { (error) -> Void in
                    if((error) != nil) {
                        print("VPN Preferences error: 2")
                        if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false {
                            if self.isConnectedToVpn == true  {
                                print("VPN is Activeted")
                            }else{
                                self.startVPN()
                            }
                        }
                    }
                    else {
                        self.vpnManager.loadFromPreferences(completionHandler: { (error) in
                            if((error) != nil) {
                        print("VPN Preferences error: 2")
                                if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false {
                                    if self.isConnectedToVpn == true  {
                                        print("VPN is Activeted")
                                    }else{
                                        self.startVPN()
                                    }
                                }
                            }
                            else {
                                var startError: NSError?
                          do {
                                    try self.vpnManager.connection.startVPNTunnel()
                                }
                                catch let error as NSError {
                                    startError = error
                                }
                                catch {
                                    print("Fatal Error")
                                    fatalError()
                                }
                                if((startError) != nil) {
                                    print("VPN Preferences error: 3")
                                    if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false {
                                        if self.isConnectedToVpn == true  {
                                            print("VPN is Activeted")
                                        }else{
                                            self.startVPN()
                                        }
                                    }
                                    let alertController = UIAlertController(title: "Oops..", message:
                                        "Something went wrong while connecting to the VPN. Please try again.", preferredStyle: UIAlertController.Style.alert)
                                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
                                    self.present(alertController, animated: true, completion: nil)
                                }
                                else {
                                    self.VPNStatusDidChange(nil)
                                    print("VPN started successfully..")
                                }
                            }
                        })
                    }
                })
            }
        }
    }
    @objc func domenNameDidChange(_ notification: Notification?){
        //print("_ notification: Notification?")
         startVPN()
    }
    func theFunction(){
        struct Holder { static var called = false }
        if !Holder.called {
            Holder.called = true
            //do the thing
           startVPN()
        }
        Holder.called = false
    }
   
    @objc func VPNStatusDidChange(_ notification: Notification?) {
        print("VPN Status changed:")
        let status = self.vpnManager.connection.status
        switch status {
        case .connecting:
            touchLabel.text = "Touch to leave Trust.Zone"
           
            break
        case .connected:
            print("Connected")
         touchLabel.text = "Touch to leave Trust.Zone"
        checkConectedToVpn()
         
            break
        case .disconnecting:
            touchLabel.text = "Touch to enter Trust.Zone"
            break
        case .disconnected:
            print("Disconnected")
            touchLabel.text = "Touch to enter Trust.Zone"
            checkConectedToVpn()
            if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true {
            }
            
            break
        case .invalid:
            print("Invalid")
            
          
            break
        case .reasserting:
          
            break
        @unknown default:
            break
        }
    }


    
    func reachabilityFunc()  {
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi  {
                if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false {
                    if self.isConnectedToVpn == true  {
                    print("VPN is Activeted")
                }else{
                    self.startVPN()
                }
                     }
            }else if reachability.connection == .cellular {
                if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false{
                if self.isConnectedToVpn == true {
                    print("VPN is Activeted")
                }else{
                    self.startVPN()
                }
                }
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func startVPN(){
        DispatchQueue.main.async {
            self.switchClicked()
            self.materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
            self.materialActivityIndicator.layer.cornerRadius = 125
            self.materialActivityIndicator.startAnimating()
            self.materialActivityIndicator.rotate(duration: 3.0)
            
            self.materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
            self.materialActivityIndicator.lineWidth =  90
            self.materialActivityIndicator.layer.cornerRadius = 125
            self.materialActivityIndicator.isHidden = false
        }
        
    }

    
    
    @IBAction func pressButtonAction(sender: UIButton) {
     stopVpn = true
       if (doubleTap){
            print("doubleTap")
            doubleTap = false
       
            self.vpnManager.connection.stopVPNTunnel()
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                //                let nlogo = UserDefaults.standard.object(forKey: "logo")
                let yourImage: UIImage =  UIImage(named:  "ic_launcher")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
            })
            self.materialActivityIndicator.stopAnimating()
            self.materialActivityIndicator.isHidden = true
            self.materialActivityIndicator.lineWidth = 0
            self.progressView.stopRotating()
            
        }else {
        stopVpn = true
            doubleTap = true
           print("elsedoubleTap")
           startVPN()
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                // let nlogo = UserDefaults.standard.object(forKey: "logo")
                let yourImage: UIImage =  UIImage(named:  "ic_launcher")!
                self.img.image = yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
            })
        }
        
        
       
    }
    
    
    }
    
    

