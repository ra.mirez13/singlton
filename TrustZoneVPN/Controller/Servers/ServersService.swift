//
//  ServersService.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation

struct ServersService {
    var statusStruct = StatusStruct()

    static let shared = ServersService()
    private init() { }
   
    
   
    
    
    func getServerGroups() -> [ServerGroup] {
        let serersString = getServersString()
        let servers = parseServersString(serersString)
        return groupServers(servers)
    }
    
     func groupServers(_ servers: [Server]) -> [ServerGroup] {
        var groups = [ServerGroup]()
        var groupNames = [String]()
        
        for server in servers {
            if !groupNames.contains(server.groupName) {
                groupNames.append(server.groupName)
            }
        }
        
        for groudName in groupNames {
            let groupedServers = servers.filter({ $0.groupName == groudName })
            let group = ServerGroup(name: groudName, servers: groupedServers)
            groups.append(group)
        }
        
        return groups
    }
    
     func getServersString() -> String {
        let strsize = 10240;
        let strsizes = 10240;

        let serversStr = UnsafeMutablePointer<Int8>.allocate(capacity: strsize);
        let serversStrExpires = UnsafeMutablePointer<Int8>.allocate(capacity: strsize);
        let unsafePointerUserName = UnsafeMutablePointer<Int8>(mutating: (UserDefaults.standard.object(forKey: "login") as! NSString).utf8String)
        let unsafePointerLang = UnsafeMutablePointer<Int8>(mutating: ("EN" as NSString).utf8String)
        SslGetServerList(serversStr,  Int32(strsize),unsafePointerUserName, unsafePointerLang, serversStrExpires, Int32(strsizes))
        let expiresTime = String(cString: serversStrExpires)
        UserDefaults.standard.set(expiresTime, forKey: "expiresTime")
        statusStruct.ExpiresTime = expiresTime
       // print(String(cString: serversStr))
        return String(cString: serversStr)
    }
    
     func parseServersString(_ string: String) -> [Server] {
        let serverLines = string.components(separatedBy: "\n")
        var servers = [Server]()
        
        for line in serverLines {
            let serverProps = line.components(separatedBy: "|")
            if serverProps.count < 4 { continue }
            
            let server = Server(country: serverProps[1], nameCode: serverProps[2], serverDomen: serverProps[3], groupName: serverProps[0])
            servers.append(server)
        }
    
        return servers
    }

}
