//
//  SettingsVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import BEMCheckBox

class SettingsVC: UIViewController {
    static let shared = SettingsVC()
    @IBOutlet weak var firstCB: BEMCheckBox!
    @IBOutlet weak var secondCB: BEMCheckBox!
    @IBOutlet weak var thirdCB: BEMCheckBox!
    @IBOutlet weak var forCB: BEMCheckBox!
    @IBOutlet weak var fiveCB: BEMCheckBox!
    @IBOutlet weak var autoconnectCB: BEMCheckBox!
    var defaults = UserDefaults.standard
    var CheckBoxesArray: [BEMCheckBox]?
    var autoconnectCheckBox = false
    var first = false
    var second = true
    var third = false
    var four = false
    var five = false
    var select = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        CheckBoxesArray = [firstCB,secondCB,thirdCB,forCB,fiveCB]
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
        firstCB.delegate = self
        secondCB.delegate = self
        thirdCB.delegate = self
        forCB.delegate = self
        fiveCB.delegate = self
        

        firstCB.on = defaults.bool(forKey: Keys.fCB.rawValue)
        secondCB.on = defaults.bool(forKey: Keys.sCB.rawValue)
        thirdCB.on = defaults.bool(forKey: Keys.tCB.rawValue)
        forCB.on = defaults.bool(forKey: Keys.frCB.rawValue)
        fiveCB.on = defaults.bool(forKey: Keys.fvCB.rawValue)
        autoconnectCB.on = defaults.bool(forKey: Keys.autoconnectKeys.rawValue)

       
        if autoconnectCB.on == defaults.bool(forKey: Keys.autoconnectKeys.rawValue)   {
            autoconnectCheckBox = true
        }else{
            autoconnectCheckBox = false
        }
        
        if firstCB.on == defaults.bool(forKey: Keys.fCB.rawValue)   {
            first = true
            second = false
            third = false
            four = false
            five = false
        }else{
            first = false
        }
        if secondCB.on == defaults.bool(forKey: Keys.sCB.rawValue)   {
            second = true
            first = false
            third = false
            four = false
            five = false
        }else{
            second = false
        }
        if thirdCB.on == defaults.bool(forKey: Keys.tCB.rawValue)   {
            first = false
            second = false
            third = true
            four = false
            five = false
        }else{
            third = false
        }
        if forCB.on == defaults.bool(forKey: Keys.frCB.rawValue)   {
            four = true
            second = false
            third = false
            first = false
            five = false
        }else{
            four = false
        }
        if fiveCB.on == defaults.bool(forKey: Keys.fvCB.rawValue)   {
            five = true
            second = false
            third = false
            four = false
            first = false
            
        }else{
            five = false
        }
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.select = UserDefaults.standard.integer(forKey: "tag")
        
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    enum Keys: String {
        case  autoconnectKeys, fCB, sCB, tCB, frCB, fvCB
    }
    
    @IBAction func autoAction(_ sender: Any) {
        
        if autoconnectCB.on  {
            defaults.set(autoconnectCB.on, forKey: Keys.autoconnectKeys.rawValue)
            autoconnectCheckBox = true
            SettingsClass.share.activ = "Yes"
            defaults.set("Yes", forKey: "autoconnect")
            defaults.set(true, forKey: "trueAutoconnect")
        }else{
            defaults.removeObject(forKey: Keys.autoconnectKeys.rawValue)
            autoconnectCheckBox = false
            SettingsClass.share.activ = "No"
            defaults.set("No", forKey: "autoconnect")
            defaults.removeObject(forKey: "trueAutoconnect")

            
        }
    }
   
    

    
}
extension SettingsVC: BEMCheckBoxDelegate {
    
    
    func didTap(_ checkBox: BEMCheckBox) {
        defaults.set(autoconnectCB.on, forKey: Keys.autoconnectKeys.rawValue)

        
       let selectedIntex = checkBox.tag
        
        guard let CheckBoxesArray = CheckBoxesArray else { return }
        for box in CheckBoxesArray where box.tag != selectedIntex {
            box.on = false
            

            switch selectedIntex{
            case 0:
                print("0")
                 defaults.set(firstCB.on, forKey: Keys.fCB.rawValue)
                defaults.removeObject(forKey: Keys.sCB.rawValue)
                defaults.removeObject(forKey: Keys.tCB.rawValue)
                defaults.removeObject(forKey: Keys.frCB.rawValue)
                defaults.removeObject(forKey: Keys.fvCB.rawValue)



                
                
                
                SettingsClass.share.first = "21 (SSH, SCP, SFTP)"
                UserDefaults.standard.set("21 (SSH, SCP, SFTP)", forKey: "port")
            case 1:
                print("1")
                defaults.set(secondCB.on, forKey: Keys.sCB.rawValue)
                defaults.removeObject(forKey: Keys.fCB.rawValue)
                defaults.removeObject(forKey: Keys.tCB.rawValue)
                defaults.removeObject(forKey: Keys.frCB.rawValue)
                defaults.removeObject(forKey: Keys.fvCB.rawValue)

                 SettingsClass.share.first = "22 (FTP control)"
                UserDefaults.standard.set("22 (FTP control)", forKey: "port")
            case 2:
                print("2")
                defaults.set(thirdCB.on, forKey: Keys.tCB.rawValue)
                defaults.removeObject(forKey: Keys.sCB.rawValue)
                defaults.removeObject(forKey: Keys.fCB.rawValue)
                defaults.removeObject(forKey: Keys.frCB.rawValue)
                defaults.removeObject(forKey: Keys.fvCB.rawValue)
                SettingsClass.share.first = "80 (HTTP)"
               UserDefaults.standard.set("80 (HTTP)", forKey: "port")
            case 3:
                print("3")
                defaults.set(forCB.on, forKey: Keys.frCB.rawValue)
                defaults.removeObject(forKey: Keys.fCB.rawValue)
                defaults.removeObject(forKey: Keys.sCB.rawValue)
                defaults.removeObject(forKey: Keys.tCB.rawValue)
                defaults.removeObject(forKey: Keys.fvCB.rawValue)
                SettingsClass.share.first = "443 (HTTPS)"
              UserDefaults.standard.set("443 (HTTPS)", forKey: "port")
            case 4:
                print("4")
                defaults.set(fiveCB.on, forKey: Keys.fvCB.rawValue)
                defaults.removeObject(forKey: Keys.fCB.rawValue)
                defaults.removeObject(forKey: Keys.sCB.rawValue)
                defaults.removeObject(forKey: Keys.tCB.rawValue)
                defaults.removeObject(forKey: Keys.frCB.rawValue)
                SettingsClass.share.first = "1194 (OpenVPN)"
               UserDefaults.standard.set("1194 (OpenVPN)", forKey: "port")
            default:
                break
            }
            
        }
      

        
        
    }
    
}
