//
//  Server.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/2/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation

struct Server {
    let country: String
    let nameCode: String
    let serverDomen: String
    let groupName: String
    

    
    var domenName: String {
        let domenParts = serverDomen.components(separatedBy: ".")
        return domenParts.count > 0 ? domenParts[0] : ""
    }
    
    var flag: UIImage? {
        return UIImage(named: nameCode.lowercased() + "flags")
    }
}

struct ServerIP {
    let ip: String
    let nameCode: String
   
    var flag: UIImage? {
        return UIImage(named: nameCode.lowercased() + "flags")
    }
}
