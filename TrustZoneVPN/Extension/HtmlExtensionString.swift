//
//  HtmlExtensionString.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/17/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation
import UIKit
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
